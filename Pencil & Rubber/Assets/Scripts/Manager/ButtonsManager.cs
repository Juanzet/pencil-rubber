﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonsManager : MonoBehaviour
{
    public Text text;

    public void Level01()
    {
        SceneManager.LoadScene("Scene03GP");
    }

    public void Level02()
    {
        SceneManager.LoadScene("Scene02GP");
    }

    public void level03()
    {
        SceneManager.LoadScene("Scene01GP");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    void Start() 
    {
        text.text = Timer.instance.timerText.text;
    }

}
