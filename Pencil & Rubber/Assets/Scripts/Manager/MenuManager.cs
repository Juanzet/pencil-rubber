﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void ExitGame()
    {
        Application.Quit();
    }
    public void PlayGame()
    {
        SceneManager.LoadScene("Scene02GP");
    }

    public void BackToMenu() 
    {
        SceneManager.LoadScene("MainMenu");
    }
     public void GoToCredits() 
    {
        SceneManager.LoadScene("Credits");
    }
}
