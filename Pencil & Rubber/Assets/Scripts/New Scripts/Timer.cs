﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    // hice singleton al timer con persistencia entre escenas para despues usarlo en la escena final y mostrar el tiempo que tardo el jugador en finalizar
   public static Timer instance { get; private set; }

   float timeElapsed;
   int minutes,seconds,cents;
   public Text timerText;
   [SerializeField] GameObject destroyThis;
   
   void Awake() 
   {
        if(instance != null && instance != this) 
        {
            Destroy(destroyThis);
        }
        else 
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
   }
  
    void Update()
    {
       
        timeElapsed += Time.deltaTime;

        minutes = (int)(timeElapsed / 60f);
        seconds = (int)(timeElapsed - minutes * 60f);
        cents = (int)((timeElapsed - (int)timeElapsed) * 100f);

        timerText.text = string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, cents);

    }
}
