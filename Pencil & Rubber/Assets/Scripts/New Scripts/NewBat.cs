﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewBat : MonoBehaviour
{
    public float speed;
    public bool chase = false;
    public Transform startingPoint;
    GameObject player;
    public string levelName;

    void Start()
    {
        // refactorizar
        player = GameObject.FindGameObjectWithTag("Player");
    }

    
    void Update()
    {
        if(player == null)
            return;

        if(chase==true)
            Chase();
        else
            ReturnStartPoint();

        //Flip();
    }

    void ReturnStartPoint()
    {
        transform.position = Vector2.MoveTowards(transform.position, startingPoint.position, speed * Time.deltaTime);
    }

    void Chase()
    {
        transform.position = Vector2.MoveTowards(transform.position,player.transform.position, speed * Time.deltaTime);
        if(Vector2.Distance(transform.position, player.transform.position) <= 0.5f)
        {
            //change speed, shoot, animations
        }
        else 
        {
            //reset variables
        }
    }

    void Flip() 
    {
        if(transform.position.x > player.transform.position.x)
            transform.rotation = Quaternion.Euler(0, 0, 0);
        else 
            transform.rotation = Quaternion.Euler(0, 180, 0);
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.tag == "Player")
        {
            SceneManager.LoadScene(levelName);
        }

        if(other.gameObject.tag == "Rubber")
            Destroy(this.gameObject);
    }
}
