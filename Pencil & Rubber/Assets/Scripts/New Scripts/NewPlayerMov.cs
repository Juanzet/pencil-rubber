﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPlayerMov : MonoBehaviour
{

    //TODO: Animaciones, verificar que todo el entorno tenga el layer floor y chequear todos los saltos 
    //confirmar si se agrega el climbing o no y ver los disenios de niveles acorde a las nuevas dinamicas.
    float horizontal;
    [SerializeField] float speed = 8f;
    [SerializeField] float jumpingPower = 16f;
    bool isFacingRight = true;

    bool isJumping;

    float coyoteTime = 0.2f;
    float coyoteTimeCounter;

    float jumpBufferTime = 0.2f;
    float jumpBufferCounter;

    Animator anim;

    [SerializeField] Rigidbody2D rb;
    [SerializeField] Transform groundCheck;
    [SerializeField] LayerMask groundLayer;


    void Awake() 
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        anim.SetFloat("Vertical", vertical);
        anim.SetFloat("Horizontal", horizontal);

        CoyoteTimeAlgoritm();
        TransformToRubber();
        Flip();  
    }

    void FixedUpdate()
    {
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
    }

    void CoyoteTimeAlgoritm()
    {
        if(IsGrounded())
        {
            coyoteTimeCounter = coyoteTime;
        } 
        else 
        {
            coyoteTimeCounter -= Time.deltaTime;
        }

        if(Input.GetButtonDown("Jump")) 
        {
            jumpBufferCounter = jumpBufferTime;    
        } 
        else 
        {
            jumpBufferCounter -= Time.deltaTime;
        }

        if(coyoteTimeCounter > 0f && jumpBufferCounter > 0f) 
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpingPower);

            jumpBufferCounter = 0f;
        }

        if(Input.GetButtonUp("Jump") && rb.velocity.y > 0f) 
        {
          rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);  

          coyoteTimeCounter = 0f;
        }
  
    }
    
    private bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
    }

    private void Flip()
    {
        if (isFacingRight && horizontal < 0f || !isFacingRight && horizontal > 0f)
        {
            Vector3 localScale = transform.localScale;
            isFacingRight = !isFacingRight;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
    }
    private IEnumerator JumpCooldown()
    {
        isJumping = true;
        yield return new WaitForSeconds(0.4f);
        isJumping = false;
    }

    void TransformToRubber()
    {
        if(Input.GetKey(KeyCode.R)) 
        {
            this.gameObject.tag = "Rubber";
            anim.SetBool("IsRubber", true);
        }
        else 
        {
            anim.SetBool("IsRubber", false);
            this.gameObject.tag = "Player";
        }
       
    }
}
