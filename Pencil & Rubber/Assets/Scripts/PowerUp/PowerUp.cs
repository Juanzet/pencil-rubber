﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    Line _Line;

    public Gradient newColor;
    private bool getPowerUp = false;
  
    void Awake()
    {
        _Line = GetComponent<Line>();    
    }

    void Update()
    {
        GrabPowerUp();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            getPowerUp = true;
        }    
    }

    void GrabPowerUp()
    {
        if(getPowerUp)
        {
            _Line = Instantiate(_Line, this.transform).GetComponent<Line>();

            _Line.SetLineColor(newColor);
            _Line.SetLineWidth(25);
        }
        //_Line = Instantiate(_Line, this.transform).GetComponent<Line>();

        //_Line.SetLineColor(newColor);
        //_Line.SetLineWidth(25);
    }
}
