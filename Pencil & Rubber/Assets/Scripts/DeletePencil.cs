﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeletePencil : MonoBehaviour
{
    public GameObject img;

    private void Start() 
    {
        img.SetActive(false);
    }
    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject.CompareTag("Player"))
        {
            img.SetActive(true);
            Destroy(this.gameObject);
            
        }
    }
}
