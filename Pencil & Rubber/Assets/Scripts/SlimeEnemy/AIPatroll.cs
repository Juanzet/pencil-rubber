﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AIPatroll : MonoBehaviour
{
    public float walkSpeed;

    [HideInInspector]
    public bool mustPatrol;
    public bool mustTurn;

    public Rigidbody2D rb;
    public Transform groundCheckPos;
    public LayerMask groundLayer;
    public Text defeatTXT;
    public GameObject prefabPlayer;

    void Start()
    {
        mustPatrol = true;
    }

     
    void Update()
    {
        if(mustPatrol)
        {
            Patrol();
        }
    }
    void FixedUpdate()
    {
        if(mustPatrol)
        {
            mustTurn = Physics2D.OverlapCircle(groundCheckPos.position, 0.1f, groundLayer);
        }   
    }

    void Patrol()
    {
        if(mustTurn)
        {
            Flip();
        }

        rb.velocity = new Vector2(walkSpeed * Time.fixedDeltaTime, rb.velocity.y);
    }
    void Flip()
    {
        mustPatrol = false;
        transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        walkSpeed *= -1;
        mustPatrol = true;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            defeatTXT.text = "You Die, Press M to Reset";
            Destroy(prefabPlayer);
        }
        if (collision.tag == "Rubber")
        {
            Destroy(this.gameObject);
        }
    }

}
