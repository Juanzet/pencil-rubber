﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetGameScene03 : MonoBehaviour
{

    void Update()
    {
        if(Input.GetKey(KeyCode.M))
        {
            SceneManager.LoadScene("Scene03GP");
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            SceneManager.LoadScene("Scene01GP");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            SceneManager.LoadScene("Scene03GP");
        }
    }
}
