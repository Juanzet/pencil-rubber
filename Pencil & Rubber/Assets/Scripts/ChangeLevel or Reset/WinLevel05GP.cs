﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLevel05GP : MonoBehaviour
{
    void Update()
    {
        if(Input.GetKey(KeyCode.M))
        {
            SceneManager.LoadScene("Scene05GP");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            SceneManager.LoadScene("Scene05GP");
        }
    }
}
