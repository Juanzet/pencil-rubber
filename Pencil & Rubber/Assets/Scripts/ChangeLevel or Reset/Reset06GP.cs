﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Reset06GP : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKey(KeyCode.M))
        {
            SceneManager.LoadScene("Scene06GP");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            SceneManager.LoadScene("Win");
        }
    }
}
