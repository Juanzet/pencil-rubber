﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLevel04GP : MonoBehaviour
{  
    void Update()
    {
        ResetLevel();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            SceneManager.LoadScene("Scene05GP");
        }
    }

    void ResetLevel()
    {
        if(Input.GetKey(KeyCode.M))
        {
            SceneManager.LoadScene("Scene04GP");
        }
    }
}
