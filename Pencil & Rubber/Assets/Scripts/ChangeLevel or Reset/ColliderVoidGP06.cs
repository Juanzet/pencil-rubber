﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ColliderVoidGP06 : MonoBehaviour
{
   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            SceneManager.LoadScene("Scene06GP");
        }
    }
}