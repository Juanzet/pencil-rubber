﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour
{
	public GameObject linePrefab;
	public LayerMask cantDrawOverLayer;
	public LayerMask floor;
	int dontMoveEnemy;
	int cantDrawOverLayerIndex;
	public GameObject prefabLine02;

	[Space(30f)]
	public Gradient lineColor;
	public Gradient lineColor02;
	public float linePointsMinDistance;
	public float lineWidth;
	public float lineWidth02;
	public int index;
	public bool getPowerUp=false;

	Line currentLine;

	Camera cam;

	void Start()
	{
		cam = Camera.main;
		cantDrawOverLayerIndex = LayerMask.NameToLayer("CantDrawOver");
	}

	void Update()
	{
		//if(Input.GetKey(KeyCode.R))
		//      {
		//	getPowerUp = true;
		 //      }
		//else
		 //      {
		//	getPowerUp = false;
		 //      }

		if (Input.GetKeyDown(KeyCode.Z))
			index = 0;

			if (Input.GetMouseButtonDown(0) && index < 2)
	        {
			BeginDraw();
			index++;
		    }
			
		if (currentLine != null)
			Draw();

		if (Input.GetMouseButtonUp(0))
			EndDraw();
	}


	void BeginDraw()
    {
		if(getPowerUp)
        {
			currentLine = Instantiate(linePrefab, this.transform).GetComponent<Line>();

			currentLine.SetLineWidth(lineWidth02);
			currentLine.SetLineColor(lineColor02);
			currentLine.tag = "Rubber";
			currentLine.UsePhysics(false);
			cantDrawOverLayerIndex = LayerMask.NameToLayer("Floor");

		}
		else
        {
			currentLine = Instantiate(linePrefab, this.transform).GetComponent<Line>();

			//Set line properties
			currentLine.UsePhysics(false);
			currentLine.SetLineColor(lineColor);
			currentLine.SetPointsMinDistance(linePointsMinDistance);
			currentLine.SetLineWidth(lineWidth);
		}
		//currentLine = Instantiate(linePrefab, this.transform).GetComponent<Line>();

		////Set line properties
		//currentLine.UsePhysics(false);
		//currentLine.SetLineColor(lineColor);
		//currentLine.SetPointsMinDistance(linePointsMinDistance);
		//currentLine.SetLineWidth(lineWidth);
	}
	
	void Draw()
	{		
		Vector2 mousePosition = cam.ScreenToWorldPoint(Input.mousePosition);

		//Check if mousePos hits any collider with layer "CantDrawOver", if true cut the line by calling EndDraw( )
		RaycastHit2D hit = Physics2D.CircleCast(mousePosition, lineWidth / 3f, Vector2.zero, 1f, cantDrawOverLayer);

		if (hit)
			EndDraw();
		else
			currentLine.AddPoint(mousePosition);
	}

	void EndDraw()
	{
		if (currentLine != null)
		{
			if (currentLine.pointsCount < 2)
			{
				//If line has one point
				Destroy(currentLine.gameObject);
			}
			else
			{
				//Add the line to "CantDrawOver" layer
				currentLine.gameObject.layer = cantDrawOverLayerIndex;

				//Activate Physics on the line
				currentLine.UsePhysics(true);

				currentLine = null;
			}
		}
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            getPowerUp = true;
        }
    }


}
