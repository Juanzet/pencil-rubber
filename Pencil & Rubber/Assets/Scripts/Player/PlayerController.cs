﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _Speed = 8;
    [SerializeField] private float _JumpForce = 16;
    private Rigidbody2D _Rb;
   private bool canJump = false;
   // [SerializeField] private SpriteRenderer rubber;
    //[SerializeField] private SpriteRenderer pencil;
    [SerializeField] private Sprite rubberSprite;
    [SerializeField] private Sprite pencilSprite;
    [SerializeField] private Animator anim;
    [SerializeField] GameObject img;
     [SerializeField] GameObject refactorizar;

    //[SerializeField] ParticleSystem particle;

/*
    [Header("Nuevos Movimientos")]

     float horizontal;
    [SerializeField] float speed = 8f;
    [SerializeField] float jumpingPower = 16f;
    bool isFacingRight = true;

    bool isJumping;

    float coyoteTime = 0.2f;
    float coyoteTimeCounter;

    float jumpBufferTime = 0.2f;
    float jumpBufferCounter;

    [SerializeField] Rigidbody2D rb;
    [SerializeField] Transform groundCheck;
    [SerializeField] LayerMask groundLayer;
    */

    void Start()
    {
        img.SetActive(false);
        //_Rb = GetComponent<Rigidbody2D>();
       // rubber = GetComponent<SpriteRenderer>();
       // pencil = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }
    void Update()
    {
       PlayerMovement();

       // horizontal = Input.GetAxisRaw("Horizontal");

       // CoyoteTimeAlgoritm();
        TransformToRubber();
    }

    /// <summary>
    /// nuevos metodos 
    /// /// </summary>
    
   
   /*
    void CoyoteTimeAlgoritm()
    {
        if(IsGrounded())
        {
            coyoteTimeCounter = coyoteTime;
        } 
        else 
        {
            coyoteTimeCounter -= Time.deltaTime;
        }

        if(Input.GetButtonDown("Jump")) 
        {
            jumpBufferCounter = jumpBufferTime;    
        } 
        else 
        {
            jumpBufferCounter -= Time.deltaTime;
        }

        if(coyoteTimeCounter > 0f && jumpBufferCounter > 0f) 
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpingPower);

            jumpBufferCounter = 0f;
        }

        if(Input.GetButtonUp("Jump") && rb.velocity.y > 0f) 
        {
          rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);  

          coyoteTimeCounter = 0f;
        }
  
    }
    */
    
    /*
    private bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
    }
    */

    /// <summary>
    /// fin de los nuevos metodos
    /// </summary>

    void PlayerMovement()
    {
        
        float mov = Input.GetAxis("Horizontal");
        
        transform.position += new Vector3(mov, 0, 0) * Time.deltaTime * _Speed;

        if (!Mathf.Approximately(0, mov))
            transform.rotation = mov > 0 ? Quaternion.Euler(0, -180, 0) : Quaternion.identity;

        if (Input.GetKeyDown(KeyCode.W) && canJump == true)
        {
            _Rb.AddForce(new Vector2(0, _JumpForce), ForceMode2D.Impulse);
        }
 
    }

    void TransformToRubber()
    {
        if(Input.GetKey(KeyCode.R))
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = rubberSprite;
            this.gameObject.tag = "Rubber";
            //pencil.gameObject.SetActive(false);
            //rubber.gameObject.SetActive(true);
            //rubber.transform.position = new Vector2(pencil.transform.position.x, pencil.transform.position.y);
        } 

        if(Input.GetKeyUp(KeyCode.R))
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = pencilSprite;
            this.gameObject.tag = "Player";
            //pencil.transform.position = new Vector2(rubber.transform.position.x, rubber.transform.position.y);
            //pencil.gameObject.SetActive(true);
            //rubber.gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.CompareTag("Floor"))
        {
            canJump = true;
        }
        

        if(collision.gameObject.CompareTag("test")) 
        {
            img.SetActive(true);
            refactorizar.SetActive(false);
        }

    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            canJump = false;
        }

    }
}
