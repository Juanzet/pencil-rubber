﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController02 : MonoBehaviour
{
    private Rigidbody2D _Rb02;

    void Start()
    {
        _Rb02 = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Enemy")
        {
            Destroy(collision.gameObject);
        }
    } 
}
