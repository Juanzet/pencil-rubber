﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PercentageToWin : MonoBehaviour
{
   [SerializeField] Transform target;
   [SerializeField] Text distance;

    void Start()
    {
        distance.text = "0%";
    }

     
    void Update()
    {
       distance.text = CalculateDistance().ToString() + "%";
    }

    float CalculateDistance()
    {
        var distanceToTarget = Vector2.Distance(this.transform.position, target.transform.position);

        int calculatePercentage = (int)this.transform.position.magnitude * (int)distanceToTarget / 100;

        return calculatePercentage;
    }
}
