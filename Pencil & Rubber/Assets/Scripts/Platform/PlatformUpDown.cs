﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlatformUpDown : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            SceneManager.LoadScene("Scene02GP");

        if (collision.tag == "Rubber")
            SceneManager.LoadScene("Scene02GP");
    }
}
