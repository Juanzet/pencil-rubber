﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class RotateEnemyNivel06 : MonoBehaviour
{
    [SerializeField] Text playerDie;

    void Update()
    {
        transform.Rotate(0f, 0f, 5f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            SceneManager.LoadScene("Scene06GP");

        if (collision.tag == "Rubber")
            SceneManager.LoadScene("Scene06GP");
    }
}

